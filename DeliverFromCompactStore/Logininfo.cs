﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace DeliverFromCompactStore
{
    public class Logininfo
    {
        public string Bolag { get; set; }
        public string Usr { get; set; }
        public string Password { get; set; }
        public string GarpConfig { get; set; }
        public bool TraningMode { get; set; }
        public string SkrivLoggFil { get; set; }
        public string LogFileName { get; set; }
        public string FolderFiles { get; set; }
        public int MonthSaveLog { get; set; }
        public string FolderArchive { get; set; }
        public string FolderError { get; set; }

        public Logininfo()
        {
            Bolag = ConfigurationManager.AppSettings["Bolag"];
            Usr = ConfigurationManager.AppSettings["Användare"];
            Password = ConfigurationManager.AppSettings["Lösen"];
            GarpConfig = ConfigurationManager.AppSettings["Konfiguration"];
            if (ConfigurationManager.AppSettings["Övningsbolag"] == "True")
            {
                TraningMode = true;
            }

            if (ConfigurationManager.AppSettings["SkrivLog"] == "True")
            {
                SkrivLoggFil = ConfigurationManager.AppSettings["SkrivLog"];
            }

            LogFileName = string.Format(@"{0}"
            , Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])
            + @"\"
            + Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["FilFörLog"])
            + " "
            + DateTime.Now.ToString("yyyy-MM-dd HHmm"))
            + Path.GetExtension(ConfigurationManager.AppSettings["FilFörLog"]);

            MonthSaveLog = int.Parse(ConfigurationManager.AppSettings["AntalMånaderSparaLog"]);

            FolderFiles = string.Format(@"{0}", ConfigurationManager.AppSettings["MappInfiler"]);
            FolderArchive = string.Format(@"{0}", ConfigurationManager.AppSettings["MappArkivera"]);
            FolderError = string.Format(@"{0}", ConfigurationManager.AppSettings["MappError"]);
        }

    }
}
