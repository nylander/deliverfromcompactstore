﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace DeliverFromCompactStore
{
    public class Program
    {
        static Garp.Application GarpApp;
        static Logininfo login;
        static List<FilesFromCS> OrdersToDeliver;
        static Garp.ITable tblOGR;

        static void Main(string[] args)
        {
            try
            {
                login = new Logininfo();
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar" + Environment.NewLine);
                GarpLogin();
                ReadFiles();
                GarpDeliver();
                EndGarp();
                RemoveOldLogFiles();
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                WriteToLog("Fel " + e.Message);

            }
        }

        public static bool ReadFiles()
        {
            try
            {
                WriteToLog("Startar läsning av filer" + Environment.NewLine);
                OrdersToDeliver = new List<FilesFromCS>();
                List<FilesFromCS> FilesOK = new List<FilesFromCS>();
                List<FilesFromCS> FilesNotOK = new List<FilesFromCS>();

                foreach (string files in Directory.EnumerateFiles(login.FolderFiles))
                {
                    string fileName = Path.GetFileName(files);
                    string destFile = Path.Combine(login.FolderArchive, fileName);
                    string destErrorFile = Path.Combine(login.FolderError, fileName);

                    try
                    {
                        string line;

                        // Om bara en uppdragstyp finns i varje fil skall första läsningen bara avgöra vilken uppdragstyp det är
                        // Steg 2 blir att loopa respektive lista me uppdragstyper (filer)

                        using (StreamReader file = new StreamReader(files))
                            while ((line = file.ReadLine()) != null)
                            {
                                string[] cells = line.Split(';');

                                switch (cells[0])
                                {
                                    case "1": // Inlagring

                                        break;

                                    case "2": // Utlagrning = utleverans i Garp

                                        FilesFromCS readData = new FilesFromCS
                                        {
                                            OrderNr = cells[3].Replace(" ", string.Empty),
                                            Qty = cells[5].Replace(" ", string.Empty)
                                        };

                                        if (cells[4].Contains('-'))
                                        {
                                            string[] MtrlRow = cells[4].Split('-');
                                            readData.OrderRow = MtrlRow[0].Replace(" ", string.Empty);
                                            readData.OrderMtrlRow = MtrlRow[1].Replace(" ", string.Empty);
                                        }
                                        else
                                        {
                                            readData.OrderRow = cells[4].Replace(" ", string.Empty);
                                        }

                                        OrdersToDeliver.Add(readData);

                                        break;

                                    case "3": // Inventering

                                        break;

                                    default:

                                        // Flytta filen till Err-mapp
                                        throw new Exception();
                                }

                            }
                    }

                    catch (Exception)
                    {
                        WriteToLog("Fel vid läsning av fil " + files + Environment.NewLine);
                        // Flytta filen till Err-mappen
                        File.Move(files, destErrorFile);
                        continue;
                    }
                    File.Move(files, destFile);
                }

                return true;
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid läsning av filer" + Environment.NewLine + e);
                // Flytta filen till Err-mappen
                return false;
            }
        }

        public static void GarpDeliver()
        {
            try
            {
                if (OrdersToDeliver.Count > 0)
                {
                    string OrderNr, RowNr, MtrlRowNr, OrderNr_RowNr;

                    foreach (FilesFromCS OrderRow in OrdersToDeliver)
                    {

                        OrderNr = OrderRow.OrderNr;
                        RowNr = OrderRow.OrderRow;
                        OrderNr_RowNr = Tools.fillBlankRight(OrderNr, 6) + Tools.fillBlankLeft(RowNr, 3);

                        if (OrderRow.OrderMtrlRow != null)
                        {
                            // Leverera materialrad
                            MtrlRowNr = OrderRow.OrderMtrlRow;
                            WriteToLog("Levererar materialrad " + OrderNr + "-" + RowNr + "-" + MtrlRowNr);

                            Garp.IMaterialRowDeliver MtrlDeliver = GarpApp.MaterialRowDeliver;
                            MtrlDeliver.Ordernr = OrderNr;
                            MtrlDeliver.Radnr = RowNr;
                            MtrlDeliver.MtrlRadnr = MtrlRowNr;
                            MtrlDeliver.Antal = OrderRow.Qty;
                            //deliver.Status = leverans.Status;
                            MtrlDeliver.Run();

                            WriteToLog("- OK" + Environment.NewLine);
                        }
                        else
                        {
                            // Leverera orderrad
                            WriteToLog("Levererar " + OrderNr + "-" + RowNr + " ");

                            tblOGR.Find(OrderNr_RowNr);
                            if (tblOGR.Fields.Item("LVF").Value == "5")
                            {
                                WriteToLog(" - Kan inte leverera, raden är redan levererad" + Environment.NewLine);
                            }
                            else
                            {
                                Garp.IOrderRowDeliver deliver = GarpApp.OrderRowDeliver;
                                deliver.Ordernr = OrderNr;
                                deliver.Radnr = RowNr;
                                deliver.Antal = OrderRow.Qty;
                                //deliver.Status = leverans.Status;
                                deliver.Deliver();

                                if (deliver.ErrorMsg != "0")
                                {
                                    string errorText = " - Kan inte leverera , ";

                                    switch (deliver.ErrorMsg)
                                    {
                                        case "2":
                                            errorText += "ordern finns ej!";
                                            break;
                                        case "3":
                                            errorText += "ordern ej disponibel för lev, (pga ordertyp eller spärr)!";
                                            break;
                                        case "4":
                                            errorText += "orderraden finns ej!";
                                            break;
                                        case "5":
                                            errorText += "artikeln spärrad för leverans!";
                                            break;
                                        case "11":
                                            errorText += "antal att leverera = 0 eller felaktigt format!";
                                            break;
                                        case "19":
                                            errorText += "leveransen kunde ej genomföras av servern!";
                                            break;
                                        default:
                                            errorText += "felkod: " + deliver.ErrorMsg;
                                            break;

                                    }
                                    WriteToLog(errorText + Environment.NewLine);
                                }
                                else
                                {
                                    WriteToLog("- OK" + Environment.NewLine);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid leverans av orderrad" + Environment.NewLine + e);
            }
        }

        private static void GarpLogin()
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GarpApp = new Garp.Application();

                //Loggar in i Garp och sätter bolag, användare och lösen
                WriteToLog("Loggar in i Garp. ");
                if (login.GarpConfig != null && login.GarpConfig != "")
                {
                    GarpApp.ChangeConfig(login.GarpConfig);
                }

                if (login.TraningMode == true)
                {
                    GarpApp.SwitchToTrainingMode();

                    if (GarpApp.IsTrainingMode != true)
                    {
                        GarpApp = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        WriteToLog("Kan ej logga in i Övningsbolaget!" + Environment.NewLine);

                        Environment.Exit(0);
                    }
                    else
                    {
                        WriteToLog("ÖVNINGSBOLAGET! ");
                    }
                }

                GarpApp.Login(login.Usr, login.Password);
                GarpApp.SetBolag(login.Bolag);

                if (GarpApp.User == null)
                {
                    GarpApp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine);

                    Environment.Exit(0);
                }

                WriteToLog("Inloggad som " + GarpApp.User + ", terminal " + GarpApp.Terminal + " och bolag " + GarpApp.Bolag + Environment.NewLine);
                tblOGR = GarpApp.Tables.Item("OGR");

            }
            catch (Exception e)
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message);

                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            WriteToLog("Stänger Garp" + Environment.NewLine);
        }

        private static void WriteToLog(string text)
        {
            try
            {
                Console.Write(text);
                if (login.SkrivLoggFil != null)
                {
                    File.AppendAllText(login.LogFileName, text);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                if (login.SkrivLoggFil != null)
                {
                    WriteToLog("Fel uppstod vid skrivning logfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                }
            }
        }
        public static void RemoveOldLogFiles()
        {
            try
            {
                WriteToLog("Raderar logfiler äldre än " + login.MonthSaveLog + " månader" + Environment.NewLine);
                string[] OldFilses = Directory.GetFiles(Path.GetDirectoryName(login.LogFileName));
                foreach (string file in OldFilses)
                {
                    FileInfo fileToDelete = new FileInfo(file);
                    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - login.MonthSaveLog))
                        fileToDelete.Delete();
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

    }
}
