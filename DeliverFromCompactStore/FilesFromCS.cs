﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverFromCompactStore
{
    public class FilesFromCS
    {
        public string FileName { get; set; }
        public string OrderNr { get; set; }
        public string OrderRow { get; set; }
        public string OrderMtrlRow { get; set; }
        public string Qty { get; set; }
    }
}
